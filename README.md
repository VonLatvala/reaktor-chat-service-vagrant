# Reaktor Chat Service Vagrant Scripts #

### What is this repository for? ###

* This repo includes vagrant scripts to automatically set up a server running [Reaktor Chat Service](https://bitbucket.org/VonLatvala/reaktor-chat-service)
* Version 0.1

### How do I get set up? ###

* Install Vagrant
* Install Virtualbox
* Clone this repo to a directory
* In that directory, issue the following commands:
* `cd reaktor-chat-service-vagrant && vagrant up`
* When the process completes, go to url http://localhost:4567/
* User `admin`, password `vengenfulSpirit` gets you to a preset admin account
* That should be all.

### Who do I talk to? ###

* Repo owner (Axel Latvala)