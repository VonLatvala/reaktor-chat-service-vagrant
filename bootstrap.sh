#!/usr/bin/env bash

MYSQL_ROOT_PASSWORD="crystalmaiden"

echo "Updating APT Sources"
apt-get update > /dev/null
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD"
echo "Installing required software on the VM"
apt-get install -y apache2 mysql-server php5 php5-mysqlnd git > /dev/null

if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi

if ! [ -L /var/www/html ]; then
    rm -rf /var/www/html
fi

cd /vagrant
echo "Downloading reaktor-chat-service from git"
git clone https://bitbucket.org/VonLatvala/reaktor-chat-service.git
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi

echo "Linking reaktor-chat-service to webroot"
ln -fs /vagrant/reaktor-chat-service /var/www/html
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi

echo "Randomizing database passwords"
APP_MYSQLPASS=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;`
LOGIN_MYSQLPASS=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-32};echo;`
echo "Creating a password.php configuration file"
echo "<?php
    define('APP_DATABASE_PASSWORD', '$APP_MYSQLPASS');
    define('LOGIN_DATABASE_PASSWORD', '$LOGIN_MYSQLPASS');

    define('RECAPTCHA_SITE_KEY', '6Lc_lBAUAAAAAGW2w7Szwyn1r9FJUCW1XD88xBKK');
    define('RECAPTCHA_SECRET_KEY', '6Lc_lBAUAAAAAF9RkDXHlntkXvKC9E-sO-pLlb1o');

    define('GA_TRACKING_ID', 'UA-89879033-1');" > passwords.php
echo "Downloading SQL configuration from git"
git clone https://bitbucket.org/VonLatvala/reaktor-chat-service-sql.git
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi
echo "Patching SQL configuration files with randomized passwords"

sed -i "s/APPUSERPASSWORD/$APP_MYSQLPASS/" reaktor-chat-service-sql/appuser.sql
sed -i "s/LOGINUSERPASSWORD/$LOGIN_MYSQLPASS/" reaktor-chat-service-sql/appuser.sql

echo "Committing the SQL configuration"
echo "1/4"
mysql -uroot -p$MYSQL_ROOT_PASSWORD < ./reaktor-chat-service-sql/app.sql > /dev/null
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi
echo "2/4"
mysql -uroot -p$MYSQL_ROOT_PASSWORD < ./reaktor-chat-service-sql/login.sql > /dev/null
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi
echo "3/4"
mysql -uroot -p$MYSQL_ROOT_PASSWORD < ./reaktor-chat-service-sql/appuser.sql > /dev/null
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi
echo "4/4"
mysql -uroot -p$MYSQL_ROOT_PASSWORD < ./reaktor-chat-service-sql/createadmin.sql > /dev/null
if [ $? -ne 0 ]; then
    echo "Failed"
else
    echo "Successful"
fi

echo "Patching app env vars"
sed -i "2s/.*/    define('APACHE_PATH', '\/vagrant');/" reaktor-chat-service/inc/env_vars.php
sed -i "3s/.*/    define('PROJECT_PATH', APACHE_PATH.DIRECTORY_SEPARATOR.\"reaktor-chat-service\");/" reaktor-chat-service/inc/env_vars.php
systemctl reload apache2
echo 'Setup complete! Please visit http://localhost:4567/ on the host system!'
